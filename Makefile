PROGS = all_test
OBJS = sum_array.o find_max.o fib_rec.o fib_iter.o find_str.o
CFLAGS = -g

all : ${PROGS}

sum_array.o : sum_array.s
	as -o sum_array.o sum_array.s

find_max.o : find_max.s
	as -o find_max.o find_max.s

fib_iter.o : fib_iter.s
	as -o fib_iter.o fib_iter.s

fib_rec.o : fib_rec.s
	as -o fib_rec.o fib_rec.s

find_str.o : find_str.s
	as -o find_str.o find_str.s

all_test : clean all.c sum_array.o find_max.o fib_iter.o fib_rec.o find_str.o
	gcc -o all_test all.c sum_array.o find_max.o fib_iter.o fib_rec.o find_str.o

clean :
	rm -rf ${OBJS}
