#include <stdio.h>
#include <string.h>

int sum_array_a(int *array, int n);
int find_max_a(int *array, int n);
int fib_iter_a(int n);
int fib_rec_a(int n);
int find_str_a(char *s, char *sub);

int sum_array_c(int *array, int n)
{
    int i;
    int sum = 0;

    for (i = 0; i < n; i++) {
        sum = sum + array[i];
    }

    return sum;
}

int find_max_c(int *array, int n)
{
    int i;
    int max = array[0];

    for (i = 1; i < n; i++) {
        if (array[i] > max) {
            max = array[i];
        }
    }

    return max;
}

int fib_iter_c(int n){
    int first = 0, second = 1, next, c;
    for ( c = 0 ; c <= n ; c++ ) {
        if ( c <= 1 ){
           next = c;
	}else{
           next = first + second;
           first = second;
           second = next;
        }
     }

     return next;
}

int fib_rec_c(int n)
{
    if (n == 0) {
        return 0;
    } else if (n == 1) {
        return 1;
    } else {
        return fib_rec_c(n - 1) + fib_rec_c(n - 2);
    }
}

int find_str_c(char *s, char *sub){
    int i;
    int subIndex = 0;
    int resultIndex = -1;

    if(strlen(s) < strlen(sub)){
        return resultIndex;
    }

    for (i = 0; i < strlen(s); i++ ) {
	
	if(sub[subIndex] == s[i]){
	    resultIndex = i;
	    subIndex++;
	    if(sub[subIndex] == '\0'){
                return resultIndex - strlen(sub) + 1;
            }
	}else{
	    subIndex = 0;
	    resultIndex = -1;   
	    if(s[i] == sub[0]){
            	i -= 1;
            }
	}
    }
    return resultIndex;
};

void test_for_sum_and_find_max(int *array, int array_size){
    int sum, find_max;

    sum = sum_array_c(array, array_size);
    printf("sum_array_c = %d\n", sum);

    sum = sum_array_a(array, array_size);
    printf("sum_array_a = %d\n", sum);

    find_max = find_max_c(array, array_size);
    printf("find_max_c = %d\n", find_max);

    find_max = find_max_a(array, array_size);
    printf("find_max_a = %d\n", find_max);
}

void test_for_find_str(char *s, char *sub){
    int find_str;

    find_str = find_str_c(s, sub);
    printf("find_str_c(%s, %s) = %d\n", s, sub, find_str);

    find_str = find_str_a(s, sub);
    printf("find_str_a(%s, %s) = %d\n", s, sub, find_str);
}

int main(int argc, char **argv)
{
    int sum;
    int find_max;
    int fib_iter;
    int fib_rec;
    int find_str;

    int array_size = 1000;

    int array[array_size];
    int j;

    char *s;
    char *sub;

    printf("----------------------------------TEST1--------------array from 1 to 1000 ----------------\n");

    for (j = 0; j < array_size; j++) {
       array[j] = j+1;
    }
    test_for_sum_and_find_max(array, array_size);

    printf("----------------------------------TEST2-------------array from -1000 to -1 ---------------\n");
    
    for (j = 0; j < array_size; j++) {
       array[j] = j-1000;
    }
    test_for_sum_and_find_max(array, array_size);

    printf("----------------------------------TEST3--------------array from -500 to 499 --------------\n");

    for (j = 0; j < array_size; j++) {
       array[j] = j-500;
    }
    test_for_sum_and_find_max(array, array_size);

    printf("----------------------------------TEST4---------------array from -250 to 749 --------------\n");

    for (j = 0; j < array_size; j++) {
       array[j] = j-250;
    }
    test_for_sum_and_find_max(array, array_size);

    printf("----------------------------------TEST---------------fib iter C--------------\n");
    
    for (j = 0; j < 20; j++) {
    	fib_iter = fib_iter_c(j);
    	printf("fib_iter_c(%d) = %d\n", j, fib_iter);  
    }

    printf("----------------------------------TEST---------------fib iter Assembly--------------\n");

    for (j = 0; j < 20; j++) {
	fib_iter = fib_iter_a(j);
        printf("fib_iter_a(%d) = %d\n", j, fib_iter);
    }

    printf("----------------------------------TEST---------------fib rec C--------------\n");

    for (j = 0; j < 20; j++) {
        fib_rec = fib_rec_c(j);
        printf("fib_rec_c(%d) = %d\n", j, fib_rec);
    }

    printf("----------------------------------TEST---------------fib rec Assembly--------------\n");

    for (j = 0; j < 20; j++) {
        fib_rec = fib_rec_a(j);
        printf("fib_rec_a(%d) = %d\n", j, fib_rec);
    }

    printf("---------------------------------TEST---------------find str present--------------\n");

    s = "hamburger";
    sub = "burger";     
    test_for_find_str(s, sub);

    printf("---------------------------------TEST---------------find str not present--------------\n");

    sub = "beef";
    test_for_find_str(s, sub);

    return 0;
}
